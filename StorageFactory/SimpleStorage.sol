// SPDX-License-Identifier: MIT
pragma solidity 0.8.19; //Stating our version. 

contract SimpleStorage {
     uint myFavoriteNumber;

     // uint256[] listOfFavoriteNumbers;

     struct Person{
      uint256 favoriteNumber;
      string name; 
     }

     Person[] public listOfPeople;

     Person public joe = Person({favoriteNumber: 7, name: "Joe"});

     mapping(string => uint256) public nameToNumber;

     function store(uint _favoriteNumber) public {
        myFavoriteNumber = _favoriteNumber;
     }

     function retrieve() public view returns(uint256) {
        return myFavoriteNumber;
     }

     function addPerson(string memory _name, uint256 _favoriteNumber) public {
         listOfPeople.push(Person({favoriteNumber: _favoriteNumber, name: _name}));
         nameToNumber[_name] = _favoriteNumber;
     }

}