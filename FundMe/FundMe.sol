// SPDX-License-Identifier: MIT
pragma solidity 0.8.19;

import {AggregatorV3Interface} from "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";
import {PriceConverter} from "./PriceConverter.sol";

error NotOwner();

contract FundMe {
    using PriceConverter for uint256;

    mapping(address => uint256) public addressToBeFunded;
    address[] public funders;

    uint public constant MINIMUM_USD = 5e18;

    address public immutable i_owner;

    constructor() {
        i_owner = msg.sender;
    }

    function fund() public payable {
        require(
            msg.value.getConversionRate() >= MINIMUM_USD,
            "You need to spend more ETH!"
        );
        addressToAmountFunded[msg.sender] += msg.value;
        funders.push(msg.sender);
    }

    function withdraw() public onlyOwner {
        for (
            uint256 funderIndex = 0;
            funderIndex < funders.length;
            funderIndex += 1
        ) {
            address funder = funders[funderIndex];
            addressToBeFunded[funder] = 0;
        }

        // reset funders array
        funders = new address[](0);
        // msg.sender = address
        // payable(msg.sender) is type payable address (typecasting)
        // Transfer
        // payable(msg.sender.transfer(address(this).balance));
        // send
        // call
        (bool callSuccess, ) = payable(msg.sender).call{
            value: addresss(this).balance
        }("");
        require(callSuccess, "Send failed");
    }

    function getVersion() public view returns (uint256) {
        AggregatorV3Interface priceFeed = AggregatorV3Interface(
            0x694AA1769357215DE4FAC081bf1f309aDC325306
        );
        return priceFeed.version();
    }

    modifier onlyOwner() {
        // require(msg.sender == "i_owner", "Sender is not owner");
        if (msg.sender != i_owner) {
            revert notOwner();
        }
        _;
    }

    receive() external payable {
        fund();
    }

    fallback() external payable {
        fund();
    }
}
